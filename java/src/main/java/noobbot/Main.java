package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.Socket;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.map.ObjectMapper;

import fi.tiam.config.RaceConfig;
import fi.tiam.message.JoinMessage;
import fi.tiam.message.MessageBase;
import fi.tiam.message.PingMessage;
import fi.tiam.racer.RaceContext;

public class Main {
	public static void main(String... args) throws IOException {
		new Main(args[0], Integer.parseInt(args[1]), args[2], args[3]);
	}

	private void init(String host, int port) throws UnsupportedEncodingException, IOException {

		socket = new Socket(host, port);
		writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

		reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

		// use Jackson instead of Gson, due to performance reasons
		//
		mapper = new ObjectMapper();
		mapper.setVisibilityChecker(mapper.getSerializationConfig().getDefaultVisibilityChecker()
				.withFieldVisibility(JsonAutoDetect.Visibility.ANY)
				.withGetterVisibility(JsonAutoDetect.Visibility.NONE)
				.withSetterVisibility(JsonAutoDetect.Visibility.NONE)
				.withCreatorVisibility(JsonAutoDetect.Visibility.ANY));
	}

	private ObjectMapper mapper;
	private Socket socket;
	private PrintWriter writer;
	private BufferedReader reader;

	public Main(String host, int port, String botName, String botKey) throws UnsupportedEncodingException, IOException {
		RaceConfig config = new RaceConfig();

		System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

		init(host, port);
		String line = null;

		// new JoinMessage(botName, botKey).send(mapper, writer);
		new JoinMessage(botName, botKey, 3).send(mapper, writer);
		RaceContext rc = new RaceContext(config);

		while ((line = reader.readLine()) != null) {
			// System.out.println("reading message: " + line);
			final MessageBase<?> msgFromServer = MessageBase.getMessage(mapper, line);
			if (msgFromServer != null) {
				msgFromServer.respond(rc, mapper, writer);
			} else {
				new PingMessage().send(mapper, writer);
			}
		}
		System.out.println("Exiting, bye!");
		socket.close();
	}

}