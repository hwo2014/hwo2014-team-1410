package fi.tiam.config;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

public class RaceConfig extends Properties {
	public RaceConfig() {
		try {
			load(new FileInputStream(new File("./config.properties")));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public double getThrottleIncreaseFactor() {
		return Double.parseDouble(getProperty("THROTTLE_INCREASE_FACTOR"));
	}

	public double getThrottleReductionFactor() {
		return Double.parseDouble(getProperty("THROTTLE_REDUCTION_FACTOR"));
	}

	public double getSafeDistancePerTick(double radius) {
		return Double.parseDouble(getProperty("SAFE_DISTANCE_PER_TICK_PER_RADIUS")) * radius;
	}

	public double getFullThrottle() {
		return Double.parseDouble(getProperty("FULL_THROTTLE"));
	}

	public double getWarningAngleForCar() {
		return Double.parseDouble(getProperty("WARNING_ANGLE_FOR_CAR"));
	}

	public double getMaxRadiusForFullThrottle() {
		return Double.parseDouble(getProperty("MAX_RADIUS_FOR_FULL_THROTTLE"));
	}

	public double getMaxAngleForCar() {
		return Double.parseDouble(getProperty("MAX_ANGLE_FOR_CAR"));
	}

}
