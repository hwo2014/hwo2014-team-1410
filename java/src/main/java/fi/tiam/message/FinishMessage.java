package fi.tiam.message;

import java.io.PrintWriter;

import org.codehaus.jackson.map.ObjectMapper;

import fi.tiam.racer.RaceContext;

public class FinishMessage extends InboundMessage<YourCarMessage.Data> {
	public FinishMessage() {
		super();
	}

	@Override
	public void respond(RaceContext rc, ObjectMapper mapper, PrintWriter out) {
		OutboundMessage<?> m = rc.finish(getData());
		if (m != null) {
			m.send(mapper, out);
		}
	}

	private String gameId;
	private int gameTick;

}
