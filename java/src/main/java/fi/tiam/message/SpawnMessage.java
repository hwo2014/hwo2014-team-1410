package fi.tiam.message;

import java.io.PrintWriter;

import org.codehaus.jackson.map.ObjectMapper;

import fi.tiam.racer.RaceContext;

public class SpawnMessage extends CrashMessage {
	public SpawnMessage() {
		super();
	}

	@Override
	public void respond(RaceContext rc, ObjectMapper mapper, PrintWriter out) {
		rc.spawn(getData());
	}
}
