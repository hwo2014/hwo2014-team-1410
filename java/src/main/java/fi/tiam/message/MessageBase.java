package fi.tiam.message;

import java.io.IOException;
import java.io.PrintWriter;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;

import fi.tiam.racer.RaceContext;

@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class MessageBase<T> {
	public MessageBase() {
	}

	private String msgType;

	private T data;

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public void respond(RaceContext rc, ObjectMapper mapper, PrintWriter out) {
		// System.out.println("responding: " + getMsgType());
	}

	public static MessageBase<?> getMessage(ObjectMapper mapper, String jsonLine) {
		MessageBase<?> msg = null;
		JsonNode rootNode;
		try {
			rootNode = mapper.readTree(jsonLine);

			String msgType = rootNode.path("msgType").asText();
			// use naming convention MsgType + Message to find the correct class
			msgType = msgType.substring(0, 1).toUpperCase() + msgType.substring(1);

			try {
				Class instanceCl = Class.forName("fi.tiam.message." + msgType + "Message");
				msg = (MessageBase<?>) mapper.readValue(jsonLine, instanceCl);
			} catch (ClassNotFoundException e) {
				System.err.println("message " + msgType + " not found! \n" + jsonLine);
			}
		} catch (JsonProcessingException e1) {
			System.err.println("Error with the server message!\n" + jsonLine);
			e1.printStackTrace();
		} catch (IOException e1) {
			System.err.println("Error with the server stream! \n" + jsonLine);
			e1.printStackTrace();
		}

		return msg;
	}

}
