package fi.tiam.message;

public class SwitchLaneMessage extends OutboundMessage<String> {
	public SwitchLaneMessage() {
		super();
	}

	public SwitchLaneMessage(boolean left) {
		super();
		setData(left ? "Left" : "Right");
	}

}
