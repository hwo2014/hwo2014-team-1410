package fi.tiam.message;

import java.io.PrintWriter;

import org.codehaus.jackson.map.ObjectMapper;

import fi.tiam.racer.RaceContext;

public class CrashMessage extends InboundMessage<CrashMessage.Data> {
	public CrashMessage() {
		super();
	}

	@Override
	public void respond(RaceContext rc, ObjectMapper mapper, PrintWriter out) {
		rc.crashReport(getData());
	}

	private String gameId, gameTick;

	public static class Data {
		private String name, color;

		public String getColor() {
			return color;
		}
	}
}
