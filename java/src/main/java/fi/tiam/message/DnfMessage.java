package fi.tiam.message;

import java.io.PrintWriter;

import org.codehaus.jackson.map.ObjectMapper;

import fi.tiam.racer.RaceContext;

public class DnfMessage extends InboundMessage<YourCarMessage.Data> {
	public DnfMessage() {
		super();
	}

	@Override
	public void respond(RaceContext rc, ObjectMapper mapper, PrintWriter out) {
		super.respond(rc, mapper, out);
		rc.disqualify(getData(), reason);
	}

	public YourCarMessage.Data getCar() {
		return car;
	}

	private YourCarMessage.Data car;

	private String reason;
	private int gameTick;

}
