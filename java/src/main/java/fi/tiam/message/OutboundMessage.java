package fi.tiam.message;

import java.io.IOException;
import java.io.PrintWriter;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

public abstract class OutboundMessage<T> extends MessageBase<T> {

	public OutboundMessage() {
		String s = getClass().getSimpleName();
		s = s.substring(0, 1).toLowerCase() + s.substring(1, s.length() - "Message".length());
		setMsgType(s);
	}

	protected boolean printMessage() {
		return false;
	}

	public void send(ObjectMapper mapper, PrintWriter out) {
		try {
			String m = mapper.writeValueAsString(this);
			if (printMessage()) {
				System.out.println(m);
			}
			out.println(m);
			out.flush();
		} catch (JsonGenerationException jge) {
			System.err.println("Error with the json message! ");
			jge.printStackTrace();
		} catch (JsonMappingException jme) {
			System.err.println("Error with the json mapping! ");
			jme.printStackTrace();
		} catch (IOException e) {
			System.err.println("Error with the connection! ");
			e.printStackTrace();
		}
	}
}
