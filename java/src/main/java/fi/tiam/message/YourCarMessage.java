package fi.tiam.message;

import java.io.PrintWriter;

import org.codehaus.jackson.map.ObjectMapper;

import fi.tiam.racer.RaceContext;

public class YourCarMessage extends InboundMessage<YourCarMessage.Data> {
	@Override
	public void respond(RaceContext rc, ObjectMapper mapper, PrintWriter out) {
		rc.setMyCar(getData());
	}

	public static class Data {
		private String name;
		private String color;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getColor() {
			return color;
		}

		public void setColor(String color) {
			this.color = color;
		}
	}
}
