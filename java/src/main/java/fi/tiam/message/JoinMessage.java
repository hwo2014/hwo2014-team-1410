package fi.tiam.message;

public class JoinMessage extends OutboundMessage<JoinMessage.Data> {

	public JoinMessage() {
		super();
	}

	public JoinMessage(String name, String key, int carCount) {
		super();
		this.carCount = carCount;
		Data d = new Data();
		d.setName(name);
		d.setKey(key);
		setData(d);
	}

	private int carCount;

	public static class Data {
		private String name, key;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getKey() {
			return key;
		}

		public void setKey(String key) {
			this.key = key;
		}
	}

}
