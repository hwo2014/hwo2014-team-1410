package fi.tiam.message;

import java.io.PrintWriter;

import org.codehaus.jackson.map.ObjectMapper;

import fi.tiam.racer.RaceContext;

public class LapFinishedMessage extends InboundMessage<LapFinishedMessage.Data> {
	public LapFinishedMessage() {
		super();
	}

	@Override
	public void respond(RaceContext rc, ObjectMapper mapper, PrintWriter out) {
		super.respond(rc, mapper, out);
		rc.lap(getData());
	}

	public static class Data {
		private YourCarMessage.Data car;

		private Time lapTime, raceTime;

		public static class Time {
			private int lap, laps, ticks;
			private long millis;

			public long getMillis() {
				return millis;
			}

			public int getTicks() {
				return ticks;
			}

			public int getLaps() {
				return laps;
			}

			public int getLap() {
				return lap;
			}
		}

		private Ranking ranking;

		public static class Ranking {
			private int overall, fastestLap;

			public int getOverall() {
				return overall;
			}

			public int getFastestLap() {
				return fastestLap;
			}

		}

		public YourCarMessage.Data getCar() {
			return car;
		}

		public Time getLapTime() {
			return lapTime;
		}

		public Time getRaceTime() {
			return raceTime;
		}

		public Ranking getRanking() {
			return ranking;
		}

	}
}
