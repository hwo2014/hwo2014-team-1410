package fi.tiam.message;

import java.util.List;

public class GameEndMessage extends InboundMessage<GameEndMessage.Data> {
	public static class Data {
		private List<Results> results;

		private static class Results {
			private YourCarMessage car;

			private Result result;

			public static class Result {
				private int ticks;
				private int laps;
				private int lap;
				private long millis;

				public long getMillis() {
					return millis;
				}

				public void setMillis(long millis) {
					this.millis = millis;
				}

				public int getLaps() {
					return laps;
				}

				public void setLaps(int laps) {
					this.laps = laps;
				}

				public int getTicks() {
					return ticks;
				}

				public void setTicks(int ticks) {
					this.ticks = ticks;
				}

				public int getLap() {
					return lap;
				}

				public void setLap(int lap) {
					this.lap = lap;
				}
			}

			public Result getResult() {
				return result;
			}

			public void setResult(Result result) {
				this.result = result;
			}

		}

		private List<BestLaps> bestLaps;

		public static class BestLaps {
			private YourCarMessage car;
			private fi.tiam.message.GameEndMessage.Data.Results.Result result;

			public fi.tiam.message.GameEndMessage.Data.Results.Result getResult() {
				return result;
			}

			public void setResult(
					fi.tiam.message.GameEndMessage.Data.Results.Result result) {
				this.result = result;
			}

		}

		public List<Results> getResults() {
			return results;
		}

		public void setResults(List<Results> results) {
			this.results = results;
		}

		public List<BestLaps> getBestLaps() {
			return bestLaps;
		}

		public void setBestLaps(List<BestLaps> bestLaps) {
			this.bestLaps = bestLaps;
		}
	}
}
