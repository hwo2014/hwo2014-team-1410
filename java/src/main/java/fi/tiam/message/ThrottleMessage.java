package fi.tiam.message;

public class ThrottleMessage extends OutboundMessage<Double> {

	private int gameTick;

	public ThrottleMessage() {
		super();
	}

	public ThrottleMessage(Double d, int tick) {
		super();
		// spawn message doesn't have tick
		if (tick > 0) {
			this.gameTick = tick;
		}
		super.setData(d);
	}
}
