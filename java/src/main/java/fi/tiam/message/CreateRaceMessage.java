package fi.tiam.message;

public class CreateRaceMessage extends OutboundMessage<CreateRaceMessage.Data> {

	public CreateRaceMessage() {
		super();
	}

	public CreateRaceMessage(String track, String pw, String botName, String botKey, int cars) {
		this();
		password = pw;
		trackName = track;
		setData(new Data());
		getData().botId = new JoinMessage.Data();
		getData().botId.setKey(botKey);
		getData().botId.setName(botName);
		carCount = cars;
	}

	private String password, trackName;
	private int carCount;

	public static class Data {
		private JoinMessage.Data botId;
	}
}
