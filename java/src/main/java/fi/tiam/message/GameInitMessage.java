package fi.tiam.message;

import java.io.PrintWriter;
import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.ObjectMapper;

import fi.tiam.racer.RaceContext;

public class GameInitMessage extends InboundMessage<GameInitMessage.Data> {
	@Override
	public void respond(RaceContext rc, ObjectMapper mapper, PrintWriter out) {
		rc.createTrack(getData());
	}

	public static class Data {
		private Race race;

		public static class Race {
			private Track track;

			public static class Track {
				private String id;
				private String name;
				private List<Piece> pieces;

				public static class Piece {
					private double length;
					@JsonProperty("switch")
					private boolean switchLane;
					private Double radius;
					private Double angle;
					private boolean bridge;

					public Double getAngle() {
						return angle;
					}

					public boolean isSwitchLane() {
						return switchLane;
					}

					public Double getRadius() {
						return radius;
					}

					public double getLength() {
						return length;
					}

				}

				private List<Lanes> lanes;

				public static class Lanes {
					private double distanceFromCenter;
					private int index;

					public int getIndex() {
						return index;
					}

					public double getDistanceFromCenter() {
						return distanceFromCenter;
					}

				}

				private StartingPoint startingPoint;

				public static class StartingPoint {
					private Position position;

					public static class Position {
						private double x;
						private double y;

						public double getY() {
							return y;
						}

						public double getX() {
							return x;
						}

					}

					private double angle;

					public double getAngle() {
						return angle;
					}

					public Position getPosition() {
						return position;
					}
				}

				public String getId() {
					return id;
				}

				public String getName() {
					return name;
				}

				public StartingPoint getStartingPoint() {
					return startingPoint;
				}

				public List<Piece> getPieces() {
					return pieces;
				}

				public List<Lanes> getLanes() {
					return lanes;
				}
			}

			private List<Car> cars;

			public List<Car> getCars() {
				return cars;
			}

			public RaceSession getRaceSession() {
				return raceSession;
			}

			public static class Car {

				private fi.tiam.message.YourCarMessage.Data id;

				private Dimensions dimensions;

				public static class Dimensions {
					private double length;
					private double width;
					private double guideFlagPosition;

					public double getLength() {
						return length;
					}

					public double getGuideFlagPosition() {
						return guideFlagPosition;
					}

					public double getWidth() {
						return width;
					}
				}

				public fi.tiam.message.YourCarMessage.Data getId() {
					return id;
				}

				public Dimensions getDimensions() {
					return dimensions;
				}

			}

			private RaceSession raceSession;

			public static class RaceSession {
				private int laps;
				private long maxLapTimeMs;
				private boolean quickRace;

				public boolean isQuickRace() {
					return quickRace;
				}

				public long getMaxLapTimeMs() {
					return maxLapTimeMs;
				}

				public int getLaps() {
					return laps;
				}

			}

			public Track getTrack() {
				return track;
			}
		}

		public Race getRace() {
			return race;
		}

	}
}
