package fi.tiam.message;

import java.io.PrintWriter;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;

import fi.tiam.racer.RaceContext;

public class CarPositionsMessage extends
		InboundMessage<List<CarPositionsMessage.Data>> {
	@Override
	public void respond(RaceContext rc, ObjectMapper mapper, PrintWriter out) {
		OutboundMessage<?> mes = rc.newPositions(getData(), gameTick);
		mes.send(mapper, out);
	}

	private String gameId;
	private int gameTick;

	public static class Data {
		private YourCarMessage.Data id;
		private double angle;

		private PiecePosition piecePosition;

		public static class PiecePosition {
			private int pieceIndex;
			private double inPieceDistance;
			private Lane lane;
			private int lap;

			public static class Lane {
				private int startLaneIndex;
				private int endLaneIndex;

				public int getEndLaneIndex() {
					return endLaneIndex;
				}

				public int getStartLaneIndex() {
					return startLaneIndex;
				}
			}

			public int getPieceIndex() {
				return pieceIndex;
			}

			public int getLap() {
				return lap;
			}

			public Lane getLane() {
				return lane;
			}

			public double getInPieceDistance() {
				return inPieceDistance;
			}
		}

		public YourCarMessage.Data getId() {
			return id;
		}

		public double getAngle() {
			return angle;
		}

		public PiecePosition getPiecePosition() {
			return piecePosition;
		}

	}
}
