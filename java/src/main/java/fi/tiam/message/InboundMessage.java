package fi.tiam.message;

import java.io.PrintWriter;

import org.codehaus.jackson.map.ObjectMapper;

import fi.tiam.racer.RaceContext;

public abstract class InboundMessage<T> extends MessageBase<T> {
	@Override
	public void respond(RaceContext rc, ObjectMapper mapper, PrintWriter out) {
		new PingMessage().send(mapper, out);
	}
}
