package fi.tiam.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fi.tiam.message.CarPositionsMessage;
import fi.tiam.model.RacersModel.CarModel;
import fi.tiam.model.TrackModel.LaneModel;
import fi.tiam.model.TrackModel.PieceModel;

public class PositionModel {
	private TrackModel track;

	public PositionModel(TrackModel track) {
		this.track = track;
	}

	private Map<String, CarPositionModel> cars = new HashMap<String, CarPositionModel>();
	private boolean hasTurnedOnThisPiece;

	public void setPosition(CarPositionsMessage.Data position, RacersModel racers, boolean hasTurnedOnThisPiece) {
		this.hasTurnedOnThisPiece = hasTurnedOnThisPiece;
		CarPositionModel cm = new CarPositionModel(racers.getCar(position.getId().getColor()), track.getPiece(position
				.getPiecePosition().getPieceIndex()), position.getAngle(), position.getPiecePosition()
				.getInPieceDistance(), position.getPiecePosition().getLap(), position.getPiecePosition().getLane()
				.getStartLaneIndex(), position.getPiecePosition().getLane().getEndLaneIndex());
		cars.put(position.getId().getColor(), cm);
	}

	public class CarPositionModel {
		private CarModel car;
		private double angle, pieceDistance;
		private PieceModel piece;
		private int lap, laneStart, laneEnd;

		@Override
		public String toString() {
			return "angle: " + angle + " pieceDistance: " + pieceDistance + ", lap: " + lap + ", laneStart: "
					+ laneStart + " laneEnd: " + laneEnd;
		}

		public double getPieceDistance() {
			return pieceDistance;
		}

		public CarPositionModel(CarModel car, PieceModel piece, double angle, double pieceDistance, int lap,
				int laneStart, int laneEnd) {
			this.car = car;
			this.piece = piece;
			this.angle = angle;
			this.pieceDistance = pieceDistance;
			this.lap = lap;
			this.laneStart = laneStart;
			this.laneEnd = laneEnd;
			car.setCurrentPosition(this);
			car.setAngle(angle);
		}

		public PieceModel getPiece() {
			return piece;
		}

		public void crash(boolean raceMode, List<CarPositionModel> path) {
			getCurrentLane().crash(this, car, raceMode, path);
		}

		private int getLane() {
			if (hasTurnedOnThisPiece) {
				return laneEnd;
			} else {
				return laneStart;
			}
		}

		public LaneModel getCurrentLane() {
			return getPiece().getLanes()[getLane()];
		}

		public double getAngle() {
			return angle;
		}

	}

}
