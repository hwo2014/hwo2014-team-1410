package fi.tiam.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fi.tiam.config.RaceConfig;
import fi.tiam.message.GameInitMessage.Data.Race.Car;
import fi.tiam.model.PositionModel.CarPositionModel;
import fi.tiam.model.TrackModel.LaneModel;

public class RacersModel {

	private Map<String, CarModel> cars = new HashMap<String, CarModel>();
	private String myCarColor;
	private RaceConfig config;

	public RacersModel(String myCarColor, RaceConfig config) {
		this.myCarColor = myCarColor;
		this.config = config;
	}

	public void addCar(Car car) {
		cars.put(car.getId().getColor(), new CarModel(car));
	}

	public CarModel getCar(String color) {
		return cars.get(color);
	}

	public CarModel getMyCar() {
		return getCar(myCarColor);
	}

	public class CarModel {
		private boolean isMyCar;
		private String name, color;
		private double length, width, guideFlagPosition;
		private CarPositionModel currentPosition;

		private double throttle, speed, previousSpeed, previousDistance;
		private double angle, previousAngle;
		private int lap = 1;

		public int getLap() {
			return lap;
		}

		public void setLap(int lap) {
			this.lap = lap;
		}

		private boolean active;

		public boolean isActive() {
			return active;
		}

		@Override
		public String toString() {
			return "name: " + name + " color: " + color + " mycar: " + isMyCar + " throttle: " + throttle;
		}

		public void crash(boolean raceMode, List<CarPositionModel> path) {
			if (currentPosition != null) {
				currentPosition.crash(raceMode, path);
				active = false;
			}
		}

		public void spawn() {
			active = true;
		}

		CarModel(Car car) {
			this.isMyCar = myCarColor.equals(car.getId().getColor());
			this.name = car.getId().getName();
			this.color = car.getId().getColor();
			length = car.getDimensions().getLength();
			width = car.getDimensions().getWidth();
			guideFlagPosition = car.getDimensions().getGuideFlagPosition();
			active = true;
			throttle = 1;
		}

		public CarPositionModel getCurrentPosition() {
			return currentPosition;
		}

		public void setCurrentPosition(CarPositionModel currentPosition) {
			this.currentPosition = currentPosition;
		}

		public double getThrottle() {
			return throttle;
		}

		public void setThrottle(LaneModel lane, double throttle, double roadRadius, double nextPieceRadius,
				boolean crashed) {
			this.throttle = throttle;
		}

		public void setThrottle2(LaneModel lane, double throttle, double roadRadius, double nextPieceRadius,
				boolean crashed) {
			if ((active || crashed)) {
				if (currentPosition.getPieceDistance() > this.previousDistance) {
					this.previousSpeed = speed;
					speed = currentPosition.getPieceDistance() - this.previousDistance;
					this.previousDistance = currentPosition.getPieceDistance();
				} else {
					this.previousDistance = 0;
				}
				if (roadRadius > 0 && getDistancePerTick() > 0) {
					double ticksForThisPieceLeft = getTicksLeft(lane);
					if (ticksForThisPieceLeft * getAngleRatePerTick() < config.getMaxAngleForCar() - angle) {
						throttle *= config.getThrottleIncreaseFactor();
					} else {
						System.out.println("ticks are going to run out");
						throttle = 0;
					}
				}
				// slow down if the next one is an angle
				else if (nextPieceRadius <= config.getMaxRadiusForFullThrottle() && nextPieceRadius > 0
						&& lane.getTurnAngleDirection() != lane.getNextLane().getTurnAngleDirection()) {
					if (config.getSafeDistancePerTick(nextPieceRadius) < getDistancePerTick()) {
						System.out.println("next curve, slow down: " + config.getSafeDistancePerTick(nextPieceRadius)
								+ " < " + getDistancePerTick());
						throttle = 0;
					}
				}
				if (roadRadius > 0) {
					System.out.println("roadRadius: " + roadRadius + "car angle: " + angle + ", prev angle: "
							+ previousAngle + ", throttle: " + throttle + ", distance/tick " + getDistancePerTick());
				}
			}
			if (crashed) {
				throttle = config.getFullThrottle();
			}
			if (throttle < 0) {
				throttle = 0;
			}
			if (throttle > 1 || roadRadius >= config.getMaxRadiusForFullThrottle() || roadRadius == 0) {
				throttle = config.getFullThrottle();
			}

			this.throttle = throttle;
		}

		private double getTicksLeft(LaneModel lane) {
			double d = (lane.getPiece().getLength(lane) - currentPosition.getPieceDistance()) / getDistancePerTick();
			if (lane.getNextLane().isAngle()
					&& lane.getTurnAngleDirection() == lane.getNextLane().getTurnAngleDirection()) {
				d = (lane.getPiece().getLength(lane.getNextLane()) + lane.getPiece().getLength(lane) - currentPosition
						.getPieceDistance()) / getDistancePerTick();
			}
			return d;
		}

		private double getAngleRatePerTick() {
			return angle - previousAngle;
		}

		private double getDistancePerTick() {
			return speed;
		}

		public boolean isMyCar() {
			return isMyCar;
		}

		public double getAngle() {
			return angle;
		}

		public void setAngle(double angle) {
			this.previousAngle = this.angle;
			this.angle = Math.abs(angle);
		}

		public long getLapTime() {
			return lapTime;
		}

		public void setLapTime(long lapTime) {
			this.lapTime = lapTime;
		}

		public int getRanking() {
			return ranking;
		}

		public void setRanking(int ranking) {
			this.ranking = ranking;
		}

		private long lapTime;
		private int ranking;

	}

}
