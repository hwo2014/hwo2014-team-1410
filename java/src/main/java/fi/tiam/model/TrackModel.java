package fi.tiam.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import fi.tiam.config.RaceConfig;
import fi.tiam.message.GameInitMessage.Data.Race;
import fi.tiam.message.GameInitMessage.Data.Race.Track.Lanes;
import fi.tiam.message.GameInitMessage.Data.Race.Track.Piece;
import fi.tiam.model.PositionModel.CarPositionModel;
import fi.tiam.model.RacersModel.CarModel;

public class TrackModel implements Serializable {

	private static final long serialVersionUID = -7383531549299148011L;
	private String id;
	private RaceConfig config;

	private TrackModel(String id, Race race, RaceConfig config) {
		this.id = id;
		this.config = config;
		track = new ArrayList<PieceModel>();
		PieceModel previous = null;
		int i = 0;
		for (Piece p : race.getTrack().getPieces()) {
			++i;
			previous = addPiece(p, race.getTrack().getLanes(), previous, race.getTrack().getPieces().size() == i);
		}
	}

	public static TrackModel getInstance(String id, Race race, RaceConfig config) {
		TrackModel t = loadTrack(id);
		if (t == null) {
			t = new TrackModel(id, race, config);
		}
		t.config = config;
		return t;
	}

	private static TrackModel loadTrack(String id) {
		File f = new File("./tracks/" + id);
		FileInputStream fin;
		ObjectInputStream fis = null;
		TrackModel t = null;
		if (f.exists()) {
			try {
				fin = new FileInputStream(f);
				fis = new ObjectInputStream(fin);
				t = (TrackModel) fis.readObject();
			} catch (Exception e) {
				System.err.println("Error loading track: " + id + ", " + f.getAbsolutePath());
			} finally {
				if (fis != null) {
					try {
						fis.close();
					} catch (IOException e) {
						System.err.println("Error closing stream, track id: " + id);
					}
				}
			}
		}
		return t;
	}

	public void saveTrack() {
		File f = new File("./tracks/" + id);

		FileOutputStream fout;
		ObjectOutputStream oos = null;
		try {
			f.createNewFile();
			fout = new FileOutputStream(f);
			oos = new ObjectOutputStream(fout);
			oos.writeObject(this);
		} catch (Exception e) {
			System.err.println("Error saving track: " + id + "  path: " + f.getAbsolutePath());
			e.printStackTrace();
		} finally {
			if (oos != null) {
				try {
					oos.close();
				} catch (IOException e) {
					System.err.println("Error closing stream, track id: " + id);
				}
			}
		}
	}

	private List<PieceModel> track;

	public PieceModel addPiece(Piece t, List<Lanes> lanes, PieceModel previous, boolean lastPiece) {
		PieceModel pm;
		if (t.getAngle() == null) {
			pm = new PieceModel(t.getLength(), previous, lastPiece);
		} else {
			pm = new AngleModel(previous, t.getLength(), t.getRadius(), t.getAngle(), lastPiece);
		}
		track.add(pm);
		LaneModel[] l = new LaneModel[lanes.size()];
		pm.setLanes(l);
		for (int i = 0; i < lanes.size(); ++i) {
			l[i] = new LaneModel(pm, t.isSwitchLane(), lanes.get(i).getDistanceFromCenter(), lanes.get(i).getIndex());
		}
		return pm;
	}

	public PieceModel getPiece(int index) {
		return track.get(index);
	}

	public int getTrackLength() {
		return track.size();
	}

	public class LaneModel implements Comparable<LaneModel>, Serializable {

		private static final long serialVersionUID = -1819994618318836467L;
		private double distanceFromCenter;
		private int index;
		private double suggestedSpeed;
		private double maxSpeed;
		private PieceModel piece;
		private boolean switchable;

		public int getIndex() {
			return index;
		}

		public String toString() {
			return "Lane index: " + index + ", maxSpeed: " + maxSpeed + " switchable: " + switchable + ". "
					+ piece.toString();
		}

		public int getTurnAngleDirection() {
			if (isAngle()) {
				return ((AngleModel) piece).getAngleSign();
			}
			return 0;
		}

		LaneModel(PieceModel piece, boolean switchable, double distanceFromCenter, int index) {
			this.distanceFromCenter = distanceFromCenter;
			this.index = index;
			this.piece = piece;
			this.switchable = switchable;
			suggestedSpeed = config.getFullThrottle();
			maxSpeed = config.getFullThrottle();
		}

		public PieceModel getPiece() {
			return piece;
		}

		public boolean isSwitchable() {
			return switchable;
		}

		public LaneModel getLeftLane() {
			if (switchable) {
				if (index > 0) {
					return piece.getLanes()[index - 1];
				}
			}
			return null;
		}

		public LaneModel getRightLane() {
			if (switchable) {
				if (piece.getLanes().length > (index + 1)) {
					return piece.getLanes()[index + 1];
				}
			}
			return null;
		}

		public Vector<LaneModel> getPossibleRoutes() {
			Vector<LaneModel> v = new Vector<>();
			if (getLeftLane() != null) {
				v.add(getLeftLane());
			}
			if (getRightLane() != null) {
				v.add(getRightLane());
			}
			if (piece.next != null) {
				v.add(getNextLane());
			}
			return v;
		}

		public LaneModel getNextLane() {
			return piece.next.lanes[index];
		}

		public double getDistance() {
			return piece.getLaneWeight(this);
		}

		@Override
		public int compareTo(LaneModel o) {
			return Double.compare(getDistance(), o.getDistance());
		}

		public void crash(CarPositionModel position, CarModel car, boolean raceMode, List<CarPositionModel> path) {
			maxSpeed *= 0.95;

			for (int i = path.size() - 1; i >= 0; --i) {
				path.get(i).getCurrentLane().maxSpeed *= 0.95;
				if (!path.get(i).getCurrentLane().isAngle()) {
					break;
				}
			}

			System.err.println("crashed with throttle: " + car.getThrottle() + " and position: " + position.toString());
			System.err.println("crashed with piece: " + piece.toString() + " lane: " + toString());

			if (car.isMyCar()) {
				setSuggestedLaneSpeed(position, car, raceMode, true);
			}
			// save values for further use
			saveTrack();
		}

		public void setSuggestedLaneSpeed(CarPositionModel position, CarModel car, boolean raceMode, boolean crashed) {
			double nextRadius = getNextLane().piece instanceof AngleModel ? ((AngleModel) getNextLane().piece)
					.getRadius(getNextLane()) : 0;
			double currentRadius = isAngle() ? ((AngleModel) piece).getRadius(this) : 0;
			car.setThrottle(this, maxSpeed, currentRadius, nextRadius, crashed);
		}

		public boolean isAngle() {
			return piece instanceof AngleModel;
		}
	}

	public class PieceModel implements Serializable {

		private static final long serialVersionUID = -590975129660764758L;
		private double length;
		private PieceModel previous, next;
		private boolean lastPiece, firstPiece;

		PieceModel(double length, PieceModel previous, boolean lastPiece) {
			this.length = length;
			this.previous = previous;
			this.lastPiece = lastPiece;
			if (previous != null) {
				previous.next = this;
				firstPiece = true;
			}
			if (this.lastPiece) {
				track.get(0).previous = this;
				next = track.get(0);
			}
		}

		public PieceModel getNext() {
			return next;
		}

		public int getIndex() {
			return track.indexOf(this);
		}

		private LaneModel[] lanes;

		public LaneModel[] getLanes() {
			return lanes;
		}

		public void setLanes(LaneModel[] lanes) {
			this.lanes = lanes;
		}

		/**
		 * factor to define the time penalty on the piece
		 * 
		 * @return
		 */
		public double getLaneWeight(LaneModel lane) {
			return 0;
		}

		@Override
		public String toString() {
			return "Straight piece length : " + length + " index: " + getIndex();
		}

		public double getLength(LaneModel lane) {
			return length;
		}
	}

	class AngleModel extends PieceModel implements Serializable {

		private static final long serialVersionUID = 3077989074951174493L;
		double radius, angle;

		AngleModel(PieceModel previous, double length, double radius, double angle, boolean lastPiece) {
			super(length, previous, lastPiece);
			this.angle = angle;
			this.radius = radius;
		}

		public int getAngleSign() {
			return Integer.signum((int) angle);
		}

		public boolean isTurnLeft() {
			return angle < 0;
		}

		public double getRadius(LaneModel lane) {
			double r = lane.distanceFromCenter + radius;
			return r;
		}

		@Override
		public double getLength(LaneModel lane) {
			// pie 2*r /360degrees * angle to get the length of the lane
			double r = getRadius(lane);
			return 3.14 * 2 * r / 360 * Math.abs(angle);
		}

		/**
		 * weight is used for defining the fastest route
		 */
		@Override
		public double getLaneWeight(LaneModel lane) {
			// inner lanes have shorter distances
			boolean preferInnedLane = getRadius(lane) >= config.getMaxRadiusForFullThrottle();

			// prefer the outer lanes, radius is bigger=> speed can be bigger
			if (isTurnLeft() || (!isTurnLeft() && preferInnedLane)) {
				return lane.getIndex();
			} else {
				return getLanes()[getLanes().length - 1 - lane.getIndex()].getIndex();
			}
		}

		@Override
		public String toString() {
			return "Index: " + track.indexOf(this) + ", Angle: " + angle + ", radius: " + radius + " angle: " + angle
					+ " index: " + getIndex();
		}
	}

}
