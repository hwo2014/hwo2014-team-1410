package fi.tiam.racer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;

import fi.tiam.model.PositionModel.CarPositionModel;
import fi.tiam.model.TrackModel;
import fi.tiam.model.TrackModel.LaneModel;
import fi.tiam.model.TrackModel.PieceModel;

public class Dijkstra {
	/**
	 * Uses Dijkstra's algorithm to define the shortest path in the track
	 * 
	 * @param track
	 * @param piece
	 * @param lane
	 */
	public List<LaneModelVertex> calculateOptimalRoute(TrackModel track, final CarPositionModel position,
			final LaneModel lane, int piecesForward) {
		buildVertexes(track, position.getPiece());
		// get the piece where to calculate the optimal route
		PieceModel targetModel = position.getPiece();
		for (int i = 0; i < piecesForward; ++i) {
			targetModel = targetModel.getNext();
		}
		computePaths(vertexes.get(lane));

		LaneModelVertex optimalTarget = null;
		// path to all possible lanes
		for (int i = 0; i < targetModel.getLanes().length; ++i) {
			LaneModelVertex temp = vertexes.get(targetModel.getLanes()[i]);
			if (optimalTarget == null || optimalTarget.getMinDistance() > temp.getMinDistance()) {
				optimalTarget = temp;
			}
			// favor middle lanes, since they have more options to turn
			if (optimalTarget.getMinDistance() == temp.getMinDistance() && temp.getModel().getLeftLane() != null
					&& temp.getModel().getRightLane() != null) {
				optimalTarget = temp;
			}
			// System.out.println("Distance to " + temp + ": " +
			// temp.minDistance);
			// List<LaneModelVertex> path = getShortestPathTo(temp);
			// System.out.println("Path: " + path);
		}
		// remove first node since it is the current track
		List<LaneModelVertex> path = getShortestPathTo(optimalTarget);
		path.remove(0);
		return path;
	}

	private Map<LaneModel, LaneModelVertex> vertexes;

	private void buildVertexes(TrackModel track, final PieceModel piece) {
		vertexes = new HashMap<>();
		// build vertexes
		PieceModel pm = piece;
		for (int i = 0; i < track.getTrackLength(); ++i) {
			for (int j = 0; j < pm.getLanes().length; ++j) {
				LaneModel lm = pm.getLanes()[j];
				LaneModelVertex v = new LaneModelVertex(lm);
				vertexes.put(lm, v);
			}
			pm = pm.getNext();
		}
	}

	private List<LaneModelVertex> getShortestPathTo(LaneModelVertex target) {
		List<LaneModelVertex> path = new ArrayList<LaneModelVertex>();
		for (LaneModelVertex vertex = target; vertex != null; vertex = vertex.previous)
			path.add(vertex);
		Collections.reverse(path);
		return path;
	}

	public static class LaneModelVertex implements Comparable<LaneModelVertex> {
		public LaneModel getModel() {
			return model;
		}

		public String toString() {
			return "distance: " + getMinDistance() + " lane: [" + model.toString() + "]";
		};

		LaneModelVertex(LaneModel model) {
			this.model = model;
			reset();
		}

		private LaneModel model;
		private double minDistance;

		public void reset() {
			minDistance = Double.POSITIVE_INFINITY;
		}

		public double getMinDistance() {
			return minDistance;
		}

		public void setMinDistance(double minDistance) {
			this.minDistance = minDistance;
		}

		private LaneModelVertex previous;

		public LaneModelVertex getPrevious() {
			return previous;
		}

		public void setPrevious(LaneModelVertex previous) {
			this.previous = previous;
		}

		public int compareTo(LaneModelVertex other) {
			return Double.compare(minDistance, other.minDistance);
		}
	}

	public void computePaths(LaneModelVertex source) {
		source.minDistance = 0;
		PriorityQueue<LaneModelVertex> vertexQueue = new PriorityQueue<LaneModelVertex>();
		vertexQueue.add(source);

		while (!vertexQueue.isEmpty()) {
			LaneModelVertex u = vertexQueue.poll();

			// Visit each LaneModel exiting u
			for (LaneModel e : u.model.getPossibleRoutes()) {
				LaneModelVertex v = vertexes.get(e);
				double weight = e.getDistance();
				double distanceThroughU = u.getMinDistance() + weight;
				if (distanceThroughU < v.getMinDistance()) {
					vertexQueue.remove(v);
					v.setMinDistance(distanceThroughU);
					v.setPrevious(u);
					vertexQueue.add(v);
				}
			}
		}
	}
}
