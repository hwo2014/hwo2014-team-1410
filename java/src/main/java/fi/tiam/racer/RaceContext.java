package fi.tiam.racer;

import java.util.ArrayList;
import java.util.List;

import fi.tiam.config.RaceConfig;
import fi.tiam.message.CarPositionsMessage;
import fi.tiam.message.CrashMessage;
import fi.tiam.message.GameInitMessage;
import fi.tiam.message.GameInitMessage.Data.Race.Car;
import fi.tiam.message.LapFinishedMessage;
import fi.tiam.message.OutboundMessage;
import fi.tiam.message.SwitchLaneMessage;
import fi.tiam.message.ThrottleMessage;
import fi.tiam.message.YourCarMessage;
import fi.tiam.model.PositionModel;
import fi.tiam.model.PositionModel.CarPositionModel;
import fi.tiam.model.RacersModel;
import fi.tiam.model.RacersModel.CarModel;
import fi.tiam.model.TrackModel;
import fi.tiam.model.TrackModel.LaneModel;

public class RaceContext {

	private String myCarColor;
	private TrackModel track;
	private RacersModel racers;
	private RaceConfig config;

	public RaceContext(RaceConfig congig) {
		this.config = congig;
	}

	public void setMyCar(YourCarMessage.Data data) {
		this.myCarColor = data.getColor();
	}

	public void createTrack(GameInitMessage.Data data) {
		track = TrackModel.getInstance(data.getRace().getTrack().getId(), data.getRace(), config);
		System.out.println("Race inited. Cars:");
		racers = new RacersModel(myCarColor, config);
		for (Car car : data.getRace().getCars()) {
			racers.addCar(car);
			System.out.println(racers.getCar(car.getId().getColor()));
		}

	}

	private CarPositionModel currentPosition;
	private boolean hasTurnedOnThisPiece;
	private Dijkstra ai = new Dijkstra();

	private List<CarPositionModel> path = new ArrayList<>();

	public OutboundMessage<?> newPositions(List<CarPositionsMessage.Data> data, int gameTick) {
		PositionModel positions = new PositionModel(track);
		for (CarPositionsMessage.Data p : data) {
			positions.setPosition(p, racers, hasTurnedOnThisPiece);
		}
		CarPositionModel myPosition = racers.getMyCar().getCurrentPosition();
		if (currentPosition != null && !myPosition.getPiece().equals(currentPosition.getPiece())) {
			hasTurnedOnThisPiece = false;
			System.out.println(myPosition.getCurrentLane() + ", " + myPosition);
			path.add(myPosition);
		}

		currentPosition = myPosition;
		LaneModel target = currentPosition.getCurrentLane();
		// if we can switch the lane, check the route options
		if (currentPosition.getCurrentLane().isSwitchable() && !hasTurnedOnThisPiece) {
			target = ai
					.calculateOptimalRoute(track, myPosition, myPosition.getCurrentLane(), track.getTrackLength() - 1)
					.get(0).getModel();
		}

		if (target.equals(myPosition.getCurrentLane().getLeftLane()) && !hasTurnedOnThisPiece) {
			hasTurnedOnThisPiece = true;
			System.out.println("Turn left");
			return new SwitchLaneMessage(true);
		}
		if (target.equals(myPosition.getCurrentLane().getRightLane()) && !hasTurnedOnThisPiece) {
			hasTurnedOnThisPiece = true;
			System.out.println("Turn right");
			return new SwitchLaneMessage(false);
		}

		return getThrottle(gameTick);
	}

	boolean raceMode = false;

	private ThrottleMessage getThrottle(int gameTick) {
		currentPosition.getCurrentLane().setSuggestedLaneSpeed(currentPosition, racers.getMyCar(), raceMode, false);
		return new ThrottleMessage(racers.getMyCar().getThrottle(), gameTick);
	}

	public void crashReport(CrashMessage.Data data) {
		racers.getCar(data.getColor()).crash(raceMode, path);
	}

	public OutboundMessage<?> spawn(CrashMessage.Data data) {
		racers.getCar(data.getColor()).spawn();
		return getThrottle(-1);
	}

	public void lap(LapFinishedMessage.Data data) {
		CarModel car = racers.getCar(data.getCar().getColor());
		car.setLap(data.getLapTime().getLap());
		car.setLapTime(data.getLapTime().getMillis());
		car.setRanking(data.getRanking().getOverall());
		if (car.isMyCar()) {
			System.out.println("MY LAP FINISHED!");
		}
		System.out.println("LAP TIME: " + car.getLapTime() + ", ranking: " + car.getRanking());
	}

	public OutboundMessage<?> finish(YourCarMessage.Data data) {
		if (racers.getCar(data.getColor()).isMyCar()) {
			System.out.println("MY RACE FINISHED!");
			return new ThrottleMessage(0d, -1);
		}
		return null;
	}

	public void disqualify(YourCarMessage.Data data, String reason) {
		if (racers.getCar(data.getColor()).isMyCar()) {
			System.out.println("RACE OVER!!");
		}
		System.out.println("Car disqualified: " + data.getColor() + ", " + reason);
		racers.getCar(data.getColor()).crash(true, path);
	}
}
